#!/usr/bin/python
# add 10Gb to a user's quota

import sys,getopt
import imaplib

def get_quota(mbox):
    quota_response=c.getquota(mbox)
    if quota_response[0]=='OK':
        resource=quota_response[1][0][len(mbox)+1:]
        quota_now=resource.strip('()').split()[2]
        return(int(quota_now))

def usage():
    print "%s [-q amount_to_add_in_kb] username" % sys.argv[0]

quota_add=10485760 # 10Gb
user=None

admin_user = 'cyrus'
with open('/etc/cyrus-password') as pw_file:
    admin_password = pw_file.read().strip()


try:
    (opts,args)=getopt.getopt(sys.argv[1:],'q:')
except getopt.GetoptError, (detail,b):
    print detail
    usage()
    sys.exit(1)

for o in opts:
    if o[0] == '-q':
        quota_add=int(o[1])

try:
    user=args[0]
except IndexError:
    usage()
    sys.exit(1)

mbox='user/' + user


c=imaplib.IMAP4_SSL("127.0.0.1")
c.login(admin_user, admin_password)
quota_now=get_quota(mbox)
if quota_now:
    new_quota=int(quota_now)+quota_add
    print new_quota
    c.setquota(mbox,'(STORAGE {0})'.format(new_quota))
    quota_now=get_quota(mbox)
    print 'Quota for %s is now %d' % (user,quota_now)
c.logout()

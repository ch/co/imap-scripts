#!/usr/bin/python
# insert accounts from a group in AD into the local password files
# and create their home directories etc
#
# Catherine Pitt 19/03/10

import chemaccmgmt
import imaplib
import sys

admin_user = 'cyrus'
with open('/etc/cyrus-password') as pw_file:
    admin_password = pw_file.read().strip()


quota_default=10485760 # 10Gb


#hostname=os.popen('hostname -s').read().strip()

#aclname=hostname + '-users'
aclname='oldmail-users'

hostattrs=None

changed=False


# connection to department AD
l=chemaccmgmt.ADConnect.connect()

# find the object for the acl in the dept AD
try:
    g=chemaccmgmt.ADGroup(aclname,connect=l)
except chemaccmgmt.ad.NoSuchGroupError:
    print "Could not find ACL " + aclname + " in the Department AD"
    sys.exit(1)

c=imaplib.IMAP4_SSL('localhost')
c.login(admin_user, admin_password)

# FIXME
usernames={x.split()[2].split('/')[1] for x in c.list("user/%")[1]}

userlist=g.get_users_recursive(connect=l)
for u in userlist:
    username=u.get_username()
    if username not in usernames:
        print "creating user " + username
        mbox="user/" + username
        c.create(mbox)
        c.setquota(mbox,'(STORAGE {0})'.format(quota_default))


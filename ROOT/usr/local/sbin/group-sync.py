#!/usr/bin/python
# To create shared mailboxes in the IMAP server
#
# RFL 2016-05-25

import chemaccmgmt
import imaplib
import sys

group_quota_default=10485760 # 10Gb
user_quota_default= 1048576  # 1Gb for users created as part of a group
readonly=False
user_acl_default='lrswipkxtecd'
admin_acl='lrswipkxtecda'

admin_user='cyrus'
with open('/etc/cyrus-password') as pw_file:
    admin_password = pw_file.read().strip()


def parseacl(acl):
	a=acl.split()
	# Remove first item
	a.reverse()
	a.pop()
	a.reverse()
	m=len(a)
	d={}
	for i in range(m/2):
        	d[a[i*2]]=list(a[i*2+1])
	return d


def ensureuserexists(login=None):
	result=imapcmd(imap.list('user/'+login))
	if result[0] is None:
		createmailbox('user/'+login,user_quota_default)

def imapcmd(imapresult):
	if (imapresult[0] == 'OK'):
		return imapresult[1]
	else:
		return None

def ensuremailbox(mbox=None):
	result=imapcmd(imap.list(mbox))
	if result[0] is None:
		createmailbox(mbox,group_quota_default)

def createmailbox(mbox=None,defquota=1024):
	if readonly:
		print("Would create mailbox "+mbox+" with quota "+format(defquota))
	else:
		result=imapcmd(imap.create(mbox))
		if result is not None:
			imap.setquota(mbox,'(STORAGE {0})'.format(defquota))
			setacl(mbox,admin_user,admin_acl)
		else:
			print("Failed to create "+mbox)

def setacl(mbox,user,acl):
	if not readonly:
		result=imapcmd(imap.setacl(mbox,user,acl))
	else:
		print("Grant "+user+" "+acl+" on "+mbox)

def grantuserrightsongroupmailbox(mbox,user):
	acl=getacl(mbox)
	if user not in acl:
		setacl(mbox,user,user_acl_default)

def getacl(mbox):
	result=imapcmd(imap.getacl(mbox))
	if result is None:
		return None
	else:
		return parseacl(result[0])

def delacl(mbox,u):
	if not readonly:
		imap.deleteacl(mbox,u)
	else:
		print("Delete ACL for "+u+" from "+mbox)

def processgroup( groupcn=None):
	mbox=groupcn.replace('oldmail-','')
	ensuremailbox(mbox)
	# List of users who should have access
	group=chemaccmgmt.ADGroup(groupcn,connect=ad_connection)
        userlist=set([u.get_username() for u in group.get_users_recursive(connect=ad_connection) if u])
	# List of users who do have access
	acl=getacl(mbox)
	mboxusers=set(acl.keys())
	delusers=mboxusers.difference(userlist)
	addusers=userlist.difference(mboxusers)
	for u in addusers:
		ensureuserexists(u)
		grantuserrightsongroupmailbox(mbox,u)
	for u in delusers:
		if (u != admin_user):
			delacl(mbox,u)
	

# Search for oldmail-% groups
ad_connection=chemaccmgmt.ADConnect.connect()
search = '(&(objectClass=group)(cn=oldmail-*)(!(cn=oldmail-users)))'
filterstr = search.format()
groupcns = [x[1]['cn'][0] for x in ad_connection.large_search_s(filterstr=filterstr,attrlist=['cn']) if x[0]]

# Connect to IMAP
imap=imaplib.IMAP4_SSL('localhost')
imap.login(admin_user, admin_password)

for groupcn in groupcns:
	processgroup(groupcn)

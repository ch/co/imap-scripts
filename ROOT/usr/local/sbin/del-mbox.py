#!/usr/bin/python

import chemaccmgmt
import imaplib
import sys
import argparse

# parseacl
#   arguments:
#    acl : string such as 'user/rl201 rl201 lrswipkxtecda cyrus a'
#   returns:
#    dict of username: acl pairs, e.g. {'rl201': 'lrswipkxtecda', 'cyrus': 'a'}

def parseacl(acl):
    a=acl.split()
    a.pop(0)
    return dict(zip(a[::2], map(str, a[1::2])))

def checkacl_for_user(user):
    mbox = 'user/' + user
    acls = parseacl(cyrus.getacl(mbox)[1][0])
    allowed_users = { admin_user, user }
    users_with_acl = set(acls.keys())
    return allowed_users == users_with_acl or allowed_users > users_with_acl

def ensure_admin_acl(mbox):
    cyrus.setacl(mbox,admin_user,admin_acl)

if __name__ == '__main__':

    admin_user = 'cyrus'
    with open('/etc/cyrus-password') as pw_file:
        admin_password = pw_file.read().strip()

    admin_acl = 'lrswipkxtecda'
    groupname='oldmail-users'

    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", type=str, nargs='+', help="user(s) to delete mailbox for")
    parser.add_argument("-n", "--dry-run", action='store_true', dest='dryrun', help='dry-run mode')
    args = parser.parse_args()

    l = chemaccmgmt.ADConnect.connect()

    try:
        g = chemaccmgmt.ADGroup(groupname,connect=l)
    except chemaccmgmt.ad.NoSuchGroupError:
        print "Could not find group " + groupname + " in the Department AD"
        sys.exit(1)

    userlist = g.get_users_recursive(connect=l)

    groupmembers = {user.get_username() for user in userlist}

    users_to_maybe_delete = set()
    users_to_delete = set()

    for user in args.user:
        if user in groupmembers:
            print 'User ' + user + ' is still in the ' + groupname + ' group; refusing to delete their mailbox'
        else:
            users_to_maybe_delete.add(user)

    cyrus = imaplib.IMAP4_SSL('localhost')
    cyrus.login(admin_user, admin_password)

    extant_mailboxes = {x.split()[2].split('/')[1] for x in cyrus.list("user/%")[1]}

    for user in users_to_maybe_delete:
        if user not in extant_mailboxes:
            print 'User ' + user + ' has no mailbox; skipping'
        else:
            mbox = 'user/' + user
            if not checkacl_for_user(user):
                print 'mbox ' + mbox + ' has acls for users other than the owner and ' + admin_user + '; skipping'
            else:
                users_to_delete.add(user)

    print "going to delete: " + ", ".join(users_to_delete)

    if not args.dryrun:
        for user in users_to_delete:
            mbox = 'user/' + user
            sys.stdout.write("Deleting mbox " + mbox + '... ')
            sys.stdout.flush()
            ensure_admin_acl(mbox)
            ret = cyrus.delete(mbox)
            print ret[0]
    else:
        print "dry-run mode selected; nothing deleted"
